import React from 'react';
import ReactDOM from 'react-dom';
import WeatherForecast from "./WeatherForecast"
import store from "./app/store";
import { Provider } from 'react-redux'

ReactDOM.render(
    <Provider store={store}>
        <WeatherForecast/>
    </Provider>,
    document.getElementById('root')
);