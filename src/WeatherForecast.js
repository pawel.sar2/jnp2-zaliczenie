import React from 'react';
import styled, {ThemeProvider, createGlobalStyle} from 'styled-components'
import {useSelector} from 'react-redux'
import {Location} from "./features/location/Location";
import {ThemeSelect} from "./features/theme/theme";
import {WeatherDisplay} from "./features/weatherDisplay/WeatherDisplay";


export const StyledText = styled.div`
    color: ${props => props.theme.textColor};
`

export const StyledFrame = styled.div`
    border: 2px solid ${props => props.theme.borderColor};
    border-radius: 5px;
    margin-bottom: 2%
`

export const StyledBody = createGlobalStyle`
    body {
        background-color: ${props => props.theme.backgroundColor};
    }
`

const lightTheme = {
    textColor: "black",
    borderColor: "black",
    backgroundColor: 'white'
}

const darkTheme = {
    textColor: "white",
    borderColor: "white",
    backgroundColor: 'black'
}

const autocompleteText = 'Enter the name of the city in which you are interested in weather or localize yourself by GPS!'

const WeatherForecast = () => {
    const selectedTheme = useSelector(state => state.theme.theme)
    const theme = (selectedTheme === 'light') ? lightTheme : darkTheme

    return (
        <React.Fragment>
            <ThemeProvider theme={theme}>
                <StyledBody/>
                <ThemeSelect/>
                <br/>
                <Location text={autocompleteText} color={theme.textColor}/>
                <WeatherDisplay/>
                <br/>

                <StyledText>Uwagi: </StyledText>
                <StyledText>1. Nie do końca zrozumiałem odpowiedź, jak powinien
                    działać klasyfikator działający na dniach w trybie
                    wyświetlania godzin, więc uznałem, że w tym trybie zamiast
                    dni będzie analizować godziny. </StyledText>

                <StyledText>2. Nie wiem także, czy interpretacja polegająca na
                wyświetlaniu gifów do każdej godziny jest prawdziwa, ale chyba
                nie wpływa to bardzo na projekt</StyledText>

                <StyledText>3. Oprócz tego nie jestem pewien jak rozpoznawać dni
                deszczowe, więc po prostu sprawdzałem czy opis zawiera podciąg
                `rain`.</StyledText>

                <StyledText>4. Miałem problem z API do gifów w związku z CORS
                policy i dodałem jakieś losowe proxy, mam nadzieje, że nie nie
                przestanie nagle działać. </StyledText>
            </ThemeProvider>
        </React.Fragment>
    );
}

export default WeatherForecast
