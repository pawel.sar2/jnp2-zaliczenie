import { configureStore } from '@reduxjs/toolkit'
import locationReducer, {locationEpics} from '../features/location/locationSlice'
import themeReducer from '../features/theme/themeSlice'
import weatherDisplayReducer from '../features/weatherDisplay/weatherDisplaySlice'
import {combineEpics, createEpicMiddleware} from 'redux-observable';
import {weatherDisplayEpics} from "../features/weatherDisplay/weatherDisplaySlice";

const epicMiddleware = createEpicMiddleware();

export default configureStore({
    reducer: {
        location: locationReducer,
        theme: themeReducer,
        displayWeather: weatherDisplayReducer
    },
    middleware: [epicMiddleware]
})

const rootEpic = combineEpics(
    locationEpics,
    weatherDisplayEpics
);

epicMiddleware.run(rootEpic);