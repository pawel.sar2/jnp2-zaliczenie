import { createSlice } from '@reduxjs/toolkit'
import {combineEpics, ofType} from 'redux-observable';
import {map, mergeMap, catchError} from 'rxjs/operators';
import {ajax} from 'rxjs/ajax';
import {of} from "rxjs";

const initialState = {
    input: '',
    dict: {}
}

const locationSlice = createSlice({
    name: 'autocomplete',
    initialState,
    reducers: {
        addAutocompleteItem(state, action) {
            state.dict[action.payload.key] = action.payload.data
        },
        setGpsLocation(state, action) {
            const { position } = action.payload
            state.input = position.coords.latitude + ', ' + position.coords.longitude
        },
        setInputValue(state, action) {
            const { newInputValue } = action.payload
            state.input = newInputValue
        }
    }
})

const LOCATION_CORRECT_QUERY = 'LOCATION_CORRECT_QUERY'
const LOCATION_EMPTY_QUERY = 'LOCATION_EMPTY_QUERY'
const LOCATION_ERROR_QUERY = 'LOCATION_EMPTY_QUERY'

function parseApiResponse(key, jsonObject) {
    return {
        key: key,
        data: jsonObject.map(obj => (
            obj.name
        ))
    }
}

const autocompleteApiEpic = action$ => action$.pipe(
    ofType(LOCATION_CORRECT_QUERY),
    mergeMap(action =>
        ajax.getJSON(`http://api.weatherapi.com/v1/search.json?key=f882197452e1425e8ff205929213006&q=${action.payload}`).pipe(
            map(response => addAutocompleteItem(parseApiResponse(action.payload, response))),
            catchError(error => of({
                type: LOCATION_ERROR_QUERY,
                payload: error.xhr.response,
                error: true
            }))
        )
    )
)

export const queryAutocompleteApi = (text) => (
    text === '' ? {type: LOCATION_EMPTY_QUERY} : {type: LOCATION_CORRECT_QUERY, payload: text}
);

export const locationEpics = combineEpics(
    autocompleteApiEpic,
);

export const { addAutocompleteItem, setGpsLocation, setInputValue } = locationSlice.actions
export default locationSlice.reducer