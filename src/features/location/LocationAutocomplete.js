import React from 'react';
import Autocomplete from 'react-autocomplete'
import {useDispatch, useSelector } from 'react-redux'
import {queryAutocompleteApi, setInputValue} from './locationSlice'

function renderAutocompleteItem(item, highlighted) {
    return (
        <div key={item.label}
             style={{backgroundColor: highlighted ? '#eee' : 'white'}}>
            {item.label}
        </div>
    )
}

export const LocationAutocomplete = () => {
    const dispatch = useDispatch()
    const location = useSelector(state => state.location.input)
    const dict = useSelector(state => state.location.dict)
    const objectWithItems = (location in dict) ? dict[location].map(item => ({label: item}) ) : []

    return (
        <Autocomplete
            getItemValue={(item) => item.label}
            items={objectWithItems}
            renderItem={renderAutocompleteItem}
            value={location}
            onChange={(e) => {
                dispatch(setInputValue({newInputValue: e.target.value}))
                if (!(e.target.value in dict))
                    dispatch(queryAutocompleteApi(e.target.value))
            }}
            onSelect={(val) => {
                dispatch(setInputValue({newInputValue: val}))
                if (!(val in dict))
                    dispatch(queryAutocompleteApi(location))
            }}
        />
    )
}