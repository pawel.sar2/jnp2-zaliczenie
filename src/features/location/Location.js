import React from 'react';
import {LocationAutocomplete} from "./LocationAutocomplete";
import {useDispatch, useSelector} from "react-redux";
import {setGpsLocation} from "./locationSlice";
import {StyledFrame, StyledText} from "../../WeatherForecast";
import styled from "styled-components";
import {
    queryWeatherDisplayApi, setLocation,
    startLoadingWeather
} from "../weatherDisplay/weatherDisplaySlice";
import Loader from "react-loader-spinner";

const StyledLocationFrame = styled(props => <StyledFrame {...props} />)`
    display: table;
    padding: 1%;
    margin-bottom: 10px
`

const StyledSearch = styled.div`
    display: flex;
    justify-content: left;
    align-items: center;
    flex-direction: row;
`

const StyledButton = styled.button`
    margin-right: 1%;
`

export const Location = ({ text, color }) => {
    const dispatch = useDispatch()
    const location = useSelector(state => state.location.input)
    const {mode, loading, cachedData} = useSelector(state => state.displayWeather)

    const getLocalization = () => {
        if (navigator.geolocation)
            navigator.geolocation.getCurrentPosition(position => dispatch(setGpsLocation({position: position})))
    }

    const search = () => {
        if (location === '')
            return
        if ([location, mode] in cachedData) {
            dispatch(setLocation({location: location}))
        } else {
            dispatch(startLoadingWeather({location: location, mode: mode}))
            dispatch(queryWeatherDisplayApi(location, mode))
        }
    }

    return (
        <React.Fragment>
            <StyledLocationFrame>
                <label>
                    <StyledText>{ text } </StyledText>
                    <LocationAutocomplete />
                    <button onClick={getLocalization}>Localize me!</button>
                </label>
                <br/>
                <StyledSearch>
                    <StyledButton onClick={search}>Search!</StyledButton>
                    {loading > 0 && <Loader type="ThreeDots" color={color} height={20} width={20} />}
                </StyledSearch>
            </StyledLocationFrame>
        </React.Fragment>
    )
}