import React from "react";
import styled from "styled-components";
import {StyledText} from "../../WeatherForecast";

const StyledRadioOption = styled(props => <StyledText {...props} />)`
    display: inline;
    margin: 5px
`

export const RadioOption = ({presentedText, defaultOption, group, onClick}) => {
    const name = presentedText.toString().toLowerCase()
    return (
        <React.Fragment>
            <StyledRadioOption>
                <input type="radio" id={name} name={group}
                       value={presentedText} onClick={onClick} defaultChecked={defaultOption}/>
                <label htmlFor={name}>{presentedText}</label>
            </StyledRadioOption>
        </React.Fragment>
    )
}