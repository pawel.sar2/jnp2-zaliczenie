import React from "react";
import styled from "styled-components";
import {StyledFrame} from "../../WeatherForecast";
import {RadioOption} from "./RadioOption";

const StyledRadioGroup = styled(props => <StyledFrame {...props} />)`
    display: table-cell;
`

export const RadioGroup = ({options, defaultOption, name }) => {
    const renderedOptions = options.map(option => (
        <RadioOption key={option.name} presentedText={option.name} defaultOption={defaultOption === option.name} group={name} onClick={option.onClick}/>
    ))

    return (
        <React.Fragment>
            <StyledRadioGroup>
                <div>
                    {renderedOptions}
                </div>
            </StyledRadioGroup>
        </React.Fragment>
    )
}