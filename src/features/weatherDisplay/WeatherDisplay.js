import React from 'react';
import {useSelector} from "react-redux";
import {StyledFrame, StyledText} from "../../WeatherForecast";
import {ModeSelect} from "./Mode";
import styled from "styled-components";
import {WeatherMode} from "./Weathers/WeatherMode";

const StyledWeatherDisplayFrame = styled(props => <StyledFrame {...props} />)`
    display: table-cell;
    padding: 1%;
`

export const WeatherDisplay = () => {
    const {mode, location} = useSelector(state => state.displayWeather)

    return (
        <React.Fragment>
            <StyledWeatherDisplayFrame>
                <ModeSelect/>
                <StyledText><h1>{location}</h1></StyledText>
                <WeatherMode location={location} mode={mode}/>
            </StyledWeatherDisplayFrame>
        </React.Fragment>
    )
}