import {createSlice} from '@reduxjs/toolkit'
import {combineEpics, ofType} from 'redux-observable';
import {
    map,
    mergeMap,
    catchError,
    switchMap
} from 'rxjs/operators';
import {ajax} from 'rxjs/ajax';
import {of, interval} from "rxjs";

export const REALTIME = 'REALTIME'
export const DAILY = 'DAILY'
export const HOURLY = 'HOURLY'

const initialState = {
    mode: REALTIME,
    location: '',
    gifs: {},
    loading: 0,
    cachedData: {},
}

function parseRealtime(jsonObject) {
    const current = jsonObject.current
    const condition = current.condition
    return [{
        desc: condition.text,
        url: 'http:' + condition.icon,
        temp: current.temp_c,
        time: current.last_updated,
        actualGif: -1
    }]
}

function parseHourly(jsonObject) {
    let result = []
    const hoursWeather = jsonObject.forecast.forecastday[0].hour
    hoursWeather.forEach(item => {
        const condition = item.condition
        const hourWeather = {
            desc: condition.text,
            url: 'http:' + condition.icon,
            temp: item.temp_c,
            time: item.time,
            actualGif: -1
        }
        result.push(hourWeather)
    })
    return result
}

function parseDaily(jsonObject) {
    let result = []
    const daysWeather = jsonObject.forecast.forecastday
    daysWeather.forEach(item => {
        const day = item.day
        const condition = day.condition
        const dayWeather = {
            desc: condition.text,
            url: 'http:' + condition.icon,
            temp: day.avgtemp_c,
            maxTemp: day.maxtemp_c,
            minTemp: day.mintemp_c,
            time: item.date,
            actualGif: -1
        }
        result.push(dayWeather)
    })
    return result
}

const weatherDisplaySlice = createSlice({
    name: 'weatherDisplay',
    initialState,
    reducers: {
        startLoadingWeather(state, action) {
            const {location, mode} = action.payload
            state.cachedData[[location, mode]] = []
            state.loading++
        },
        setMode(state, action) {
            const {mode} = action.payload
            state.mode = mode
        },
        setLocation(state, action) {
            const {location} = action.payload
            state.location = location
        },
        addWeatherApiResponse: {
            reducer(state, action) {
                const {key, value, location} = action.payload
                state.cachedData[key] = value
                state.loading--
                state.location = location
            },
            prepare(jsonObject, location, mode) {
                const key = [location, mode]
                let value = {}
                if (mode === REALTIME)
                    value = parseRealtime(jsonObject)
                else if (mode === DAILY)
                    value = parseDaily(jsonObject)
                else
                    value = parseHourly(jsonObject)
                return {
                    payload: {
                        key: key,
                        value: value,
                        location: location
                    }
                }
            }
        },
        startGifLoading(state, action) {
            const {key} = action.payload
            state.loading++
            state.gifs[key] = []
        },
        addGifApiResponse: {
            reducer(state, action) {
                const {key, value} = action.payload
                state.gifs[key] = value
                state.loading--
            },
            prepare(jsonObject, desc) {
                let result = []
                jsonObject.results.forEach((item) => {
                    const {gif} = item.media[0]
                    result.push(gif.url)
                })
                return {
                    payload: {
                        key: desc,
                        value: result
                    }
                }
            }
        },
        updateGif(state, action) {
            const key = action.payload
            if (key in state.cachedData) {
                state.cachedData[key].forEach((item, i) => {
                    const desc = item.desc
                    if (desc in state.gifs && state.gifs[desc].length > 0)
                        item.actualGif = (item.actualGif + 1) % state.gifs[desc].length
                    else
                        item.actualGif = -1
                })
            }
        },
        startGifAnim(state, action) {
            const key = action.payload
            if (key in state.cachedData) {
                state.cachedData[key].forEach((item, i) => {
                    const desc = item.desc
                    if (desc in state.gifs && state.gifs[desc].length > 0)
                        item.actualGif = 0
                    state.cachedData[key][i] = item
                })
            }
        }
    }
})

const DISPLAY_CORRECT_QUERY = 'DISPLAY_CORRECT_QUERY'
const DISPLAY_EMPTY_QUERY = 'DISPLAY_EMPTY_QUERY'
const DISPLAY_ERROR_QUERY = 'DISPLAY_EMPTY_QUERY'

const weatherApiEpic = action$ => action$.pipe(
    ofType(DISPLAY_CORRECT_QUERY),
    mergeMap(action =>
        ajax.getJSON(action.payload.url).pipe(
            map(response => addWeatherApiResponse(response, action.payload.location, action.payload.mode)),
            catchError(error => of({
                type: DISPLAY_ERROR_QUERY,
                payload: error.xhr.response,
                error: true
            }))
        )
    ),
)

const realtimeQuery = (location) => ({
        url: `http://api.weatherapi.com/v1/current.json?key=f882197452e1425e8ff205929213006&q=${location}&aqi=no`,
        location: location,
        mode: REALTIME
    }
)

const daysQuery = (location, days) => ({
        url: `http://api.weatherapi.com/v1/forecast.json?key=f882197452e1425e8ff205929213006&q=${location}&days=${days}&aqi=no&alerts=no`,
        location: location,
        mode: (days === 1) ? HOURLY : DAILY
    }
)

export const queryWeatherDisplayApi = (location, mode) => {
    if (location === '')
        return {type: DISPLAY_EMPTY_QUERY}
    if (mode === REALTIME)
        return {type: DISPLAY_CORRECT_QUERY, payload: realtimeQuery(location)}
    if (mode === DAILY)
        return {type: DISPLAY_CORRECT_QUERY, payload: daysQuery(location, 3)}
    if (mode === HOURLY)
        return {type: DISPLAY_CORRECT_QUERY, payload: daysQuery(location, 1)}
}

const GIF_CORRECT_QUERY = 'GIF_CORRECT_QUERY'
const GIF_ERROR_QUERY = 'GIF_ERROR_QUERY'

const gifApiEpic = action$ => action$.pipe(
    ofType(GIF_CORRECT_QUERY),
    mergeMap(action =>
        ajax.getJSON(`https://thingproxy.freeboard.io/fetch/https://api.tenor.com/v1/search?q=${action.payload}&api_key=67P1DVR9XDMC`).pipe(
            map(response => addGifApiResponse(response, action.payload)),
            catchError(error => of({
                type: GIF_ERROR_QUERY,
                payload: error.xhr.response,
                error: true
            }))
        )
    ),
)

export const queryGifApi = (query) => ({
    type: GIF_CORRECT_QUERY,
    payload: query
})

const GIF_TIMER_START = 'GIF_TIMER_START'

const updateGifEpic = action$ => action$.pipe(
    ofType(GIF_TIMER_START),
    switchMap(action => interval(30000).pipe(
        map(() => updateGif(action.payload))
    )),
)

export const updateGifStart = (arg) => ({type: GIF_TIMER_START, payload: arg})

export const weatherDisplayEpics = combineEpics(
    weatherApiEpic,
    gifApiEpic,
    updateGifEpic,
);

export const {setMode, startLoadingWeather, addWeatherApiResponse, addGifApiResponse, updateGif, startGifAnim, setLocation, startGifLoading} = weatherDisplaySlice.actions
export default weatherDisplaySlice.reducer