import React, {useEffect} from "react";
import {WeatherFrame} from "../WeatherFrame";
import {useDispatch, useSelector} from "react-redux";
import {
    queryGifApi,
    queryWeatherDisplayApi,
    REALTIME,
    startGifAnim,
    startGifLoading,
    startLoadingWeather,
    updateGifStart
} from "../weatherDisplaySlice";
import {WeatherClassifier} from "./WeatherClassifier";

export const WeatherMode = ({ location, mode }) => {
    const dispatch = useDispatch()
    const {cachedData, gifs } = useSelector(state => state.displayWeather)

    let data = []
    if ([location, mode] in cachedData)
        data = cachedData[[location, mode]]

    useEffect(() => {
        if (location !== '') {
            if (!([location, mode] in cachedData)) {
                dispatch(startLoadingWeather({location: location, mode: mode}))
                dispatch(queryWeatherDisplayApi(location, mode))
            } else if (mode === REALTIME) {
                const array = cachedData[[location, mode]]
                if (array.length > 0) {
                    const time = array[0].time
                    const myMinutes = new Date().getMinutes()
                    if (time.length >= 2) {
                        const theirMinutes = time.substring(time.length - 2, time.length)
                        if (myMinutes - theirMinutes > 15) {
                            dispatch(startLoadingWeather({location: location, mode: mode}))
                            dispatch(queryWeatherDisplayApi(location, mode))
                        }
                    }
                }
            }
        }
    }, [location, mode]);

    useEffect(() => {
        if (data.length > 0) {
            data.forEach(item => {
                const desc = item.desc
                if (desc !== '' && !(desc in gifs)) {
                    dispatch(startGifLoading({key: desc}))
                    dispatch(queryGifApi(desc))
                }
            })
            dispatch(updateGifStart([location, mode]))
        }
    }, [data]);

    useEffect(() => {
        data.forEach(item => {
            const desc = item.desc
            const actualGif = item.actualGif
            if (desc !== '' && desc in gifs && actualGif === -1 && gifs[desc].length > 0) {
                dispatch(startGifAnim([location, mode]))
            }
        })
    }, [data, gifs]);

    const weatherFrames = (data.length === 0) ? '' : data.map((item, index) => {
        const desc = item.desc
        const actualGif = item.actualGif
        let gif = ''
        if (desc in gifs && 0 <= actualGif && actualGif < gifs[desc].length)
            gif = gifs[desc][actualGif]
        return <WeatherFrame key={"F"+index} time={item.time} desc={item.desc} url={item.url} temp={item.temp} gif={gif}/>
    })

    return (
        <React.Fragment>
            {data.length > 0 && <WeatherClassifier data={data} mode={mode}/>}
            {weatherFrames}
        </React.Fragment>
    )
}