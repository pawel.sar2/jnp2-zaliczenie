import React from "react";
import {StyledText} from "../../../WeatherForecast";
import {DAILY} from "../weatherDisplaySlice";

export const WeatherClassifier = ({ data, mode }) => {

    let averageTemp = 0
    let maxTemp = -1
    let minTemp = 100
    let rainyDays = 0

    data.forEach(item => {
        averageTemp += item.temp
        if (mode === DAILY) {
            maxTemp = Math.max(maxTemp, item.maxTemp)
            minTemp = Math.min(minTemp, item.minTemp)
        } else {
            maxTemp = Math.max(maxTemp, item.temp)
            minTemp = Math.min(minTemp, item.temp)
        }
        const parsedDesc = item.desc.toLowerCase()
        rainyDays += parsedDesc.includes('rain')
    })

    averageTemp /= data.length

    const points = (rainyDays === 0) + (18 <= averageTemp && averageTemp <= 25) +
        ((maxTemp <= 30) && (minTemp >= 15))
    const weatherMark = points === 3 ? 'nice' : (points === 2 ? 'passable' : 'not nice')

    return (
        <StyledText>The weather will be {weatherMark}</StyledText>
    )
}