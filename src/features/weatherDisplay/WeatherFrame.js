import React from "react";
import styled from "styled-components";
import {StyledText} from "../../WeatherForecast";

const StyledWeatherFrame = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
 `

const StyledWeatherImage = styled.img`
    padding-left: 5%;
    padding-right: 5%;
    max-width: 100px;
    max-height: 100px;
`

const StyledTime = styled.p`
    padding-right: 10%
`

export const WeatherFrame = ({ time, desc, url, temp, gif}) => {
    return (
        <React.Fragment>
            <StyledWeatherFrame>
                <StyledText><StyledTime>{time}</StyledTime></StyledText>
                <StyledText><p>{desc}</p></StyledText>
                <StyledWeatherImage src={url}/>
                <StyledText><p>{temp} ºC</p></StyledText>
                <StyledWeatherImage src={gif}/>
            </StyledWeatherFrame>
        </React.Fragment>
    )
}