import React from "react";
import {useDispatch} from "react-redux";
import {DAILY, HOURLY, REALTIME, setMode} from "./weatherDisplaySlice";
import {RadioGroup} from "../radio/RadioGroup";

export const ModeSelect = () => {
    const dispatch = useDispatch()

    const modeOptions = [
        { name: 'Realtime', onClick: () => dispatch(setMode({mode: REALTIME})) },
        { name: 'Daily', onClick: () => dispatch(setMode({mode: DAILY})) },
        { name: 'Hourly', onClick: () => dispatch(setMode({mode: HOURLY})) }
    ]

    return (
        <RadioGroup options={modeOptions} defaultOption={'Realtime'} name={'mode'} />
    )
}