import React from "react";
import {useDispatch} from "react-redux";
import {setTheme} from "./themeSlice";
import {RadioGroup} from "../radio/RadioGroup";

export const ThemeSelect = () => {
    const dispatch = useDispatch()

    const themeOptions = [
        { name: 'Light', onClick: () => dispatch(setTheme({theme: 'light'})) },
        { name: 'Dark', onClick: () => dispatch(setTheme({theme: 'dark'})) }
    ]

    return (
        <RadioGroup options={themeOptions} defaultOption={'Light'} name={'theme'} />
    )
}